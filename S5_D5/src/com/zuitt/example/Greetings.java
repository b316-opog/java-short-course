package com.zuitt.example;

public interface Greetings {
    public void dailyGreeting();
    public void holidayGreeting();
}

package com.zuitt.example;

public class Person implements Actions, Greetings {

    public void sleep(){
        System.out.println("zzzzzzzzzzz..");
    }

    public void run(){
        System.out.println("Running on the road.");
    }


    public void bathroom() {
        System.out.println("Flushhhhhhhh");
    }

    public void eat() {
        System.out.println("Nom nom nom.");
    }

    public void dailyGreeting(){
        System.out.println("Good mowning! Nice day for fishing aint it? ha! hah!");
    }

    public void holidayGreeting(){
        System.out.println("Merry eXams");
    }
}

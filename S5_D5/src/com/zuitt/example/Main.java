package com.zuitt.example;

public class Main {
    public static void main(String[] args){
        Person person1 = new Person();

        person1.run();
        person1.sleep();
        person1.eat();
        person1.bathroom();

        StaticPoly staticPoly = new StaticPoly();

        System.out.println(staticPoly.addition(1,5));
        System.out.println(staticPoly.addition(1,2, 10));
        System.out.println(staticPoly.addition(15.5,2.5));

        Parent parent1 = new Parent("John", 35);
        parent1.greet();
        parent1.greet("John", "Morning");
        parent1.speak();


        Child newChild = new Child();
        newChild.speak();

    }
}

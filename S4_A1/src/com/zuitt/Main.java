package com.zuitt;

import java.util.Date;

public class Main {
    public static void main(String[] args){
        User user1 = new User("Terence Gaffod", 25, "tgaff@gmail.com", "Quezon City");
        user1.call();

        Course course1 = new Course("MACQ004", "An introduction to Java for career-shifters", 15, 4000, new Date(2023,7,13), new Date(2023,7,14));
        course1.call();

    }
}

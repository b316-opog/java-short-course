package com.zuitt;

import java.lang.instrument.Instrumentation;
import java.util.Date;

public class Course extends User {

    private String courseName;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private Date endDate;
    private String instructor;


    public Course(){

    }

    public Course(String courseName, String description, int seats, double fee, Date startDate, Date endDate){
        this.courseName = courseName;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
    }



    public String getCourseName(){
        return courseName;
    }


    public String getDescription() {
        return description;
    }

    public int getSeats() {
        return seats;
    }

    public double getFee() {
        return fee;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setCourseName(String name) {
        this.courseName = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void call(){
        System.out.println("Welcome to the course " + this.courseName + ". This course can be described as " + this.description + ". Your instructor for this course is Sir " + super.getName()+ ". Enjoy!");
    }




}

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);


        System.out.println("Input an Integer whose factorial will be computed:");
        int num = 0;

        try {
            num = in.nextInt();
        }catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }


        int answer = 1;
        int answer2 = 1;
        int counter = 1;

        if(num >= 1){

            //While Loop
            while(counter <= num){
                answer *= counter;
                counter++;
            }
            System.out.println("The Factorial of " + num + " is " + answer);

            //For Loop
            for(int counter2 = 1; counter2 <= num; counter2++){
                answer2 *= counter2;
            }
            System.out.println("The Factorial of " + num + " is " + answer2);

        }else{
            System.out.println("Invalid Input!");
        }


        //For Loop


    }
}
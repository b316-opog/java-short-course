package com.zuitt.activity;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Jane","09111111111", "Quezon City");
        Contact contact2 = new Contact("John", "09222222222", "Quezon City");


        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);


        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty!");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("********************");
                System.out.println(contact.getName() + " has the following contact number:");
                System.out.println(contact.getContactNumber());
                System.out.println("********************");
                System.out.println(contact.getName() + " has been living in the following address:");
                System.out.println(contact.getAddress());
                System.out.println("=====================");
            }
        }

    }
}

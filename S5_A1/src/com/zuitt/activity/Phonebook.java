package com.zuitt.activity;

import java.util.ArrayList;
public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook(){

    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    public void setContacts(Contact contacts){
        this.contacts.add(contacts);
    }


}

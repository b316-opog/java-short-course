package com.zuitt;
import java.util.Scanner;

public class LeapYearChecker {
    public static void main(String[] args){
        Integer year;

        Scanner userInput = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");

        year = Integer.parseInt(userInput.nextLine());

        if (year % 4  == 0 || year % 400 == 0){
            System.out.println(year + " is a leap year.");
        }else{
            System.out.println(year + " is not a leap year.");
        }
    }
}

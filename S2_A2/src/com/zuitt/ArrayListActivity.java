package com.zuitt;
import java.util.ArrayList;
public class ArrayListActivity {
    public static void main(String[] args){
        java.util.ArrayList<String> friends = new java.util.ArrayList<String>();

        friends.add(0,"John");
        friends.add(1, "Jane");
        friends.add(2, "Chloe");
        friends.add(3, "Zoey");

        System.out.println("My friends are : " + friends);
    }
}

package com.zuitt;

public class PrimeNumber {
    public static void main(String[] args){
        int[] intArray = {2, 3, 5, 7, 11};

        System.out.println("The first prime number is: " + intArray[0]);
    }
}

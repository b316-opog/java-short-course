package com.zuitt;
import java.util.HashMap;
public class HashMapActivity {
    public static void main(String[] args){
        HashMap<String, Integer> items = new HashMap<String, Integer>();

        items.put("toothpaste", 15);
        items.put("toothbrush", 20);
        items.put("soap", 12);

        System.out.println("Our current inventory consists of: " + items.toString());
    }
}

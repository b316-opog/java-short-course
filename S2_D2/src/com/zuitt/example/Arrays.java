package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;


public class Arrays {
    //Declaration
    public static void main(String[] args){

        int[] intArray = new int[5];
        //the [] indicates that this int data type should be able to hold multiple int values
        //the "new" is a keyword used in non-primitive data types to tell Java to create said variable
        //this process is called "Instantiation"

        //Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};

        //Multidimensional Array
        String[][] classroom = new String[3][3];

        //Fist Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //Second Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        ArrayList<String> students = new ArrayList<String>();

        //Adding elements to the Array List
        students.add("John");
        students.add("Paul");

        //Access
        students.get(0);

        //Update
        students.set(1, "George");

        //Remove
        students.remove(1);

        //Clear
        students.clear();

        System.out.println(classroom[1]);

        //HashMap
        HashMap<String, String>job_position = new HashMap<String, String>();

        //Adding elements into the HashMap
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");

        //Access
        job_position.get("Alice");
        System.out.println(job_position.get("Alice"));

        job_position.remove("Brandon");
        System.out.println(job_position);

        //getting the keysets
        System.out.println(job_position.keySet());



    }

}

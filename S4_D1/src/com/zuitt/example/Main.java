package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

//        System.out.println("Car 2:");
//
        Car car2 = new Car();
//        car2.make = "Cevic";
//        car2.brand = "Honda";
//        car2.price = 120322;
//
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
//        System.out.println("Car 3:");
//
        Car car3 = new Car();
//        car3.make = "Yung Red";
//        car3.brand = "Sports Car";
//        car3.price = 120322;
//
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);


        Driver driver1 = new Driver("Alijandro", 25);


        car1.start();
        car2.start();
        car2.start();

        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Bugatti");
        System.out.println(car1.getMake());

        System.out.println(car2.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());



        Animal animal1 = new Animal("Clifford", "Red");
        animal1.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());

        Dog dog2 = new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();




    }
}
